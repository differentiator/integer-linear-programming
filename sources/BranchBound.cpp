#include "../headers/BranchBound.h"
#include <Eigen>
#include <cmath>
#include <limits>

using namespace Eigen;

/**
 * @desc Construtor
 *
 * @param Problem * ilp Integer Linear Programming problem to be solved by branch-and-bound.
 * @param int mode Can be: MINIMIZE, MAXIMIZE
 */
BranchBound::BranchBound(Problem *ilp, int mode) {
    this->mode = mode;
    this->root = new Node();
    this->root->ilp = ilp;
    this->foundSolution = false;
    if (mode == MAXIMIZE) {
        this->optimum = numeric_limits<double>::min();
    } else {
        this->optimum = numeric_limits<double>::max();
    }
    this->findSolutions(root);
}

/**
 * @desc Search for all solutions
 *
 * @param Node node contains the problem to be solved.
 * @returns void
 */
void BranchBound::findSolutions(Node *node) {
    //variable to check if the constraint has been added
    bool check;

    node->solver = new Simplex(this->mode, node->ilp->getObjectiveFunction(), node->ilp->getConstraints(),
                               node->ilp->getRelations());

    //checks if the problem has a solution and if it is better than the current one
    if (node->solver->hasSolution() && this->isBetterSolution(node->solver->getOptimum())) {

        //branch method
        long long pos = this->findBranch(node->solver->getSolution());

        if (pos != -1) {
            double intPart, temp;
            VectorXd newConstraint;
            //seeks the fractional part of the number
            temp = node->solver->getSolution()(pos);
            modf(temp, &intPart);

            node->left = new Node();
            node->left->ilp = new Problem(node->ilp->getObjectiveFunction(), node->ilp->getConstraints(),
                                          node->ilp->getRelations());

            //creates a new constraint based on the lower limit
            newConstraint = VectorXd::Zero(node->ilp->getConstraints().cols());
            newConstraint(pos) = 1;
            newConstraint(newConstraint.rows() - 1) = intPart;

            //if the whole part is 0 the relationship is equal
            if (intPart == 0) {
                check = node->left->ilp->addConstraint(newConstraint, 2);
            } else {
                check = node->left->ilp->addConstraint(newConstraint, 0);
            }

            if (check) {
                this->findSolutions(node->left);
            }

            node->right = new Node();
            node->right->ilp = new Problem(node->ilp->getObjectiveFunction(), node->ilp->getConstraints(),
                                           node->ilp->getRelations());

            //creates a new constraint based on the upper limit
            newConstraint(newConstraint.rows() - 1) = intPart + 1;
            check = node->right->ilp->addConstraint(newConstraint, 1);

            if (check) {
                this->findSolutions(node->right);
            }
        } else {
            this->foundSolution = true;
            this->optimum = node->solver->getOptimum();
            this->solution = node->solver->getSolution();
        }
    }
}

/**
 * @desc Search for a Real Number to Branch
 * @desc The Dakin variant technique was used
 *
 * @param VectorXd vectorToSearch vector to which the search will be performed
 * @returns __int64 Returns the column index or -1 if not found.
 */
long long BranchBound::findBranch(VectorXd vectorToSearch) {
    double intPart, floatPart = 0;
    int temp = -1;
    for (long long i = 0; i < vectorToSearch.rows(); i++) {
        if (modf(vectorToSearch(i), &intPart) > floatPart && modf(vectorToSearch(i), &intPart) < 0.99) {
            floatPart = modf(vectorToSearch(i), &intPart);
            temp = i;
        }
    }
    return temp;
}

/**
 * @desc Checks whether the current solution is better
 *
 * @param double Value to be verified
 * @returns bool true if better and false if not
 */
bool BranchBound::isBetterSolution(double optimumFound) {
    if (this->mode == MAXIMIZE && optimumFound > this->optimum) {
        return true;
    } else if (this->mode == MINIMIZE && optimumFound < this->optimum) {
        return true;
    }
    return false;
}

/**
 * @desc Returns true if the solution was found.
 * @desc Returns false otherwise.
 *
 * @returns boolean
 */
bool BranchBound::hasSolution() {
    return this->foundSolution;
}


/**
 * @desc Returns the optimal value of the maximized or minimized objective function with integer values
 *
 * @returns double
 */
double BranchBound::getOptimum() {
    return this->optimum;
}

/**
 * @desc Returns the value of the variables for the solution found.
 *
 * @returns VectorXd
 */
VectorXd BranchBound::getSolution() {
    return this->solution;
}
