#include "../headers/Exception.h"

/**
 * @desc Construtor
 * @desc Just pass an error message
 *
 * @param error_msg
 * @returns Exception
 */
Exception::Exception(string error_msg) {
    this->error_msg = error_msg;
}

/**
 * @desc Construtor
 * @desc Pass an error message and the error code
 *
 * @param error_code
 * @param error_msg
 * @returns Exception
 */
Exception::Exception(unsigned long error_code, string error_msg) {
    this->error_msg = error_msg;
    this->error_code = error_code;
}

/**
 * @desc Displays the error message on the console.
 */
void Exception::print() {
    cout << this->error_msg << endl;
}

/**
 * @desc Returns the error code.
 *
 * @returns unsigned long
 */
unsigned long Exception::getErrorCode() {
    return this->error_code;
}

/**
 * @desc Returns the exception message
 *
 * @returns string
 */
string Exception::getMessage() {
    return this->error_msg;
}
