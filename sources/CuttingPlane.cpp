#include "../headers/CuttingPlane.h"
#include <Eigen>
#include <cmath>

using namespace Eigen;

/**
 * @desc Construtor
 *
 * @param Problem * ilp Integer Linear Programming problem to be solved by the cutting plans.
 * @param int mode Can be: MINIMIZE, MAXIMIZE
 */
CuttingPlane::CuttingPlane(Problem *ilp, int mode) {
    this->mode = mode;
    this->foundSolution = false;
    this->numberOfVariables = ilp->getObjectiveFunction().rows();

    this->solver = new Simplex(this->mode, ilp->getObjectiveFunction(), ilp->getConstraints(), ilp->getRelations());

    while (true) {
        if (!this->solver->hasSolution()) {
            break;
        }

        if (this->isIntegerSolution(this->solver->getSolution())) {
            this->foundSolution = true;
            this->optimum = this->solver->getOptimum();
            this->solution = this->solver->getSolution();
            break;
        }

        if (!gomoryCut(this->solver->getTableau())) {
            break;
        }

        this->solver = new Simplex(this->mode, ilp->getObjectiveFunction(), ilp->getConstraints(), ilp->getRelations(),
                                   this->cuts);
    }
}

/*
* @desc Function to perform the cut in the solution space
* @param MatrixXd contains the tableau to be cut
* @returns bool true if the cut was inserted or false if invalid
*/
bool CuttingPlane::gomoryCut(MatrixXd tableau) {
    double intPart, floatPart;
    long long j, rowToCut;
    VectorXd cut = VectorXd::Zero(tableau.cols());

    rowToCut = getCutRow(tableau);
    if (rowToCut == -1) {
        return false;
    }

    for (j = 0; j < tableau.cols(); j++) {
        if (tableau(rowToCut, j) >= 0) {
            cut(j) = modf(tableau(rowToCut, j), &intPart);
        } else {
            floatPart = modf(tableau(rowToCut, j), &intPart);
            if (floatPart != 0) {
                cut(j) = 1 + floatPart;
            } else {
                cut(j) = 0;
            }
        }
    }

    addCut(cut);

    return true;
}

/**
 * @desc Function to return the line to be cut
 * @param MatrixXd contains the tableau to be cut
 * @returns long long the line to be cut
 */
long long CuttingPlane::getCutRow(MatrixXd tableau) {
    double intPart;
    long long i, j, row = -1;

    //line search to be performed the cut
    for (j = 0; j < this->numberOfVariables; j++) {
        for (i = 1; i < tableau.rows(); i++) {
            if (tableau(i, j) == 1) {
                if (row >= 0) {
                    row = -1;
                    break;
                } else {
                    row = i;
                    continue;
                }
            } else if (tableau(i, j) != 0) {
                row = -1;
                break;
            }
        }
        if (row != -1
            && modf(tableau(row, tableau.cols() - 1), &intPart) > 0.00001
            && modf(tableau(row, tableau.cols() - 1), &intPart) < 0.99) {
            break;
        }
        row = -1;
    }

    return row;
}

/**
 * @desc Function to add the new cut to the cut matrix
 * @param VectorXd cut to be added
 * @returns void
 */
void CuttingPlane::addCut(VectorXd cut) {
    this->cuts.conservativeResize(this->cuts.rows() + 1, cut.rows());
    this->cuts.col(this->cuts.cols() - 1) = this->cuts.col(this->cuts.cols() - 2);
    this->cuts.col(this->cuts.cols() - 2) = VectorXd::Zero(this->cuts.rows());
    this->cuts.row(this->cuts.rows() - 1) = cut;
}

/**
 * @desc Function to check if the solution is whole
 * @param VectorXd contains the solution to be checked
 * @returns bool true if the solution is whole
 */
bool CuttingPlane::isIntegerSolution(VectorXd solution) {
    double intPart;
    for (long long i = 0; i < solution.rows(); i++) {
        if (modf(solution(i), &intPart) > 0.00001 && modf(solution(i), &intPart) < 0.99) {
            return false;
        }
    }
    return true;
}

/**
 * @desc Returns true if the solution was found.
 * @desc Retorna false otherwise.
 *
 * @returns boolean
 */
bool CuttingPlane::hasSolution() {
    return this->foundSolution;
}

/**
 * @desc Returns the optimal value of the maximized or minimized objective function with integer values
 *
 * @returns double
 */
double CuttingPlane::getOptimum() {
    return this->optimum;
}

/**
 * @desc Returns the value of the variables for the solution found.
 *
 * @returns VectorXd
 */
VectorXd CuttingPlane::getSolution() {
    return this->solution;
}
