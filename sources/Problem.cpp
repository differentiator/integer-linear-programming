#include "../headers/Problem.h"

using namespace Eigen;

/**
 * @desc Construtor
 *
 * @param const VectorXd &objectiveFunction Os objective function coefficients.
 * @param const VectorXd &relations Os relation signs of restrictions {0 -> <=; 1 -> >=; 2 -> =}.
 * @param const MatrixXd &constraints Matrix with all restrictions.
 */
Problem::Problem(const VectorXd &objectiveFunction, const MatrixXd &constraints, const VectorXd &relations) {
    this->objectiveFunction = objectiveFunction;
    this->constraints = constraints;
    this->relations = relations;
}

/**
 * @desc Returns the problem constraints
 *
 * @returns MatrixXd
 */
MatrixXd Problem::getConstraints() {
    return this->constraints;
}

/**
 * @desc Returns the relations of the problem constraints
 *
 * @returns VectorXd
 */
VectorXd Problem::getRelations() {
    return this->relations;
}

/**
 * @desc Returns the objective function of the problem
 *
 * @returns VectorXd
 */
VectorXd Problem::getObjectiveFunction() {
    return this->objectiveFunction;
}

/**
 * @desc Adds a new constraint function and its relation
 *
 * @returns bool true if the constraint is added or false if the constraint already exists
 */
bool Problem::addConstraint(VectorXd constraint, int relation) {
    long long temp;
    for (temp = 0; temp < this->constraints.rows(); temp++) {
        if (((this->constraints.row(temp) - constraint.transpose()).norm() == 0) && this->relations(temp) == relation) {
            return false;
        }
    }
    this->constraints.conservativeResize(this->constraints.rows() + 1, this->constraints.cols());
    this->constraints.row(this->constraints.rows() - 1) = constraint;
    this->relations.conservativeResize(this->relations.rows() + 1);
    this->relations(this->relations.rows() - 1) = relation;
    return true;
}
