#include "../headers/Interpreter.h"
#include "../headers/Exception.h"
#include <unordered_map>

using namespace Eigen;

/**
 * @desc Construtor
 *
 * @param string fileName name of the file to be read the entry
 * @returns Interpreter
 */
Interpreter::Interpreter(const string fileName) {
    //open the file
    this->in.open(fileName.c_str());

    if (!this->in.is_open()) {
        throw (new Exception("Interpreter: It was not possible to open the input file!"));
    }

    if (fileName.length() > 3 && fileName.substr(fileName.length() - 3, fileName.length()) != ".lp") {
        this->readFile();
    } else {
        this->readLPFile();
    }

    this->pli = new Problem(this->objectiveFunction, this->constraints, this->relations);

    this->in.close();
}

/**
 * @desc Method for reading the file in free format
 * @returns void
 */
void Interpreter::readFile() {
    long long lineNumber = 1;
    string line;
    ostringstream ss;

    //reads the first line
    getline(this->in, line);

    //interprets the first line containing the objective function and mode
    this->getObjectiveAndMode(line);

    //reads the rest of the lines containing the restrictions
    while (getline(in, line)) {
        //reallocates the constraint matrix and relation vector to receive the new constraint
        this->constraints.conservativeResize(lineNumber, this->objectiveFunction.rows() + 1);
        this->constraints.row(this->constraints.rows() - 1) = VectorXd::Zero(this->objectiveFunction.rows() + 1);
        this->relations.conservativeResize(lineNumber);
        //interpreta a linha e busca a restrição
        if (!this->getConstraint(line)) {
            ss << lineNumber + 1;
            string message = "Interpreter: Line error " + ss.str() + "!";
            throw (new Exception(message));
        }
        lineNumber++;
    }
}

/**
 * @desc Method for finding objective function and mode in the string passed as a parameter
 *
 * @param string line the first line of the file containing the objective function and mode
 * @returns void
 */
void Interpreter::getObjectiveAndMode(string line) {
    string temp;
    long long counter, i;
    double variable;
    unordered_map<long long, double> variables;
    char op = '+';
    bool waitDigit = true;
    bool waitX = true;
    bool waitOperator = false;
    bool waitPos = false;

    //removes spaces
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

    temp = line.substr(0, 3);

    //transforms to uppercase
    transform(temp.begin(), temp.end(), temp.begin(), ::toupper);

    if (temp == "MAX") {
        this->mode = 2;
    } else if (temp == "MIN") {
        this->mode = 1;
    } else {
        throw (new Exception("Interpreter: No (Maximize or Minimize) mode of the objective function has been found!"));
    }

    //starts from position 6, since the first 5 are assumed to be MINZ = or MAXZ =
    counter = 5;
    variable = 1;

    if (line[counter] == '-') {
        op = '-';
        counter++;
        variable = -variable;
    }

    for (i = counter; i < line.length(); i++) {
        temp = line.substr(counter, i - counter + 1);
        if (waitDigit && isdigit(line[i]) && (!isdigit(line[i + 1]) && line[i + 1] != '.')) {
            variable = atof(temp.c_str());
            counter = i + 1;
            waitDigit = false;
            continue;
        } else if (waitX && toupper(line[i]) == 'X') {
            counter = i + 1;
            waitX = false;
            waitDigit = false;
            waitPos = true;
            continue;
        } else if (i + 1 == line.length() || (waitPos && isdigit(line[i]) && !isdigit(line[i + 1]))) {
            if (!waitPos || waitX) {
                throw (new Exception("Interpreter: Erro ao interpretar a funcao objetivo!"));
            }
            if (op == '-') {
                variable = -variable;
            }
            variables[atoll(temp.c_str())] = variable;
            variable = 1;
            counter = i + 1;
            waitOperator = true;
            waitPos = false;
            continue;
        } else if (waitOperator && (line[i] == '+' || line[i] == '-')) {
            op = line[i];
            counter = i + 1;
            waitOperator = false;
            waitDigit = true;
            waitX = true;
        }
    }

    this->objectiveFunction.resize(variables.size());

    for (i = 0; i < this->objectiveFunction.rows(); i++) {
        this->objectiveFunction(i) = variables[i + 1];
    }

}

/**
* @desc Method for fetching a PLI constraint from the passed string
*
* @param string line line of the file containing the constraint
* @returns bool true if the constraint was obtained
*/
bool Interpreter::getConstraint(string line) {
    string temp;
    long long counter, i;
    double variable;
    unordered_map<long long, double> variables;
    char op = '+';
    int relation;
    bool waitDigit = true;
    bool waitX = true;
    bool waitOperator = false;
    bool waitPos = false;

    //remove spaces
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

    counter = 0;
    variable = 1;

    if (line[counter] == '-') {
        op = '-';
        counter++;
        variable = -variable;
    }

    for (i = counter; i < line.length(); i++) {
        temp = line.substr(counter, i - counter + 1);
        if (waitDigit && isdigit(line[i]) && (!isdigit(line[i + 1]) && line[i + 1] != '.') && i + 1 != line.length()) {
            variable = atof(temp.c_str());
            counter = i + 1;
            waitDigit = false;
            continue;
        } else if (waitX && toupper(line[i]) == 'X') {
            counter = i + 1;
            waitX = false;
            waitDigit = false;
            waitPos = true;
            continue;
        } else if (waitPos && isdigit(line[i]) && !isdigit(line[i + 1]) && i + 1 != line.length()) {
            if (op == '-') {
                variable = -variable;
            }
            variables[atoll(temp.c_str())] = variable;
            variable = 1;
            counter = i + 1;
            waitOperator = true;
            waitPos = false;
            continue;
        } else if (waitOperator && (line[i] == '+' || line[i] == '-') && i + 1 != line.length()) {
            op = line[i];
            counter = i + 1;
            waitOperator = false;
            waitDigit = true;
            waitX = true;
            continue;
        } else if ((line[i] == '>' || line[i] == '=' || line[i] == '<') && i + 1 != line.length()) {
            if (!waitOperator) {
                return false;
            }
            switch (line[i]) {
                case '>':
                    relation = 1;
                    break;
                case '=':
                    relation = 2;
                    break;
                case '<':
                    relation = 0;
                    break;
            }
            if (line[i + 1] == '=') {
                i++;
            }
            if (line[i + 2] == '-') {
                op = '-';
                i++;
            } else {
                op = '+';
            }
            counter = i + 1;
            waitOperator = false;
            continue;
        } else if (i + 1 == line.length()) {
            if (waitOperator || waitDigit || waitPos || waitX) {
                return false;
            }
            variable = atof(temp.c_str());
            if (op == '-') {
                variable = -variable;
            }
            variables[this->objectiveFunction.rows() + 1] = variable;
        }
    }

    for (i = 0; i < this->constraints.cols(); i++) {
        if (variables.find(i + 1) != variables.end()) {
            this->constraints(this->constraints.rows() - 1, i) = variables[i + 1];
        }
    }

    this->relations(this->relations.rows() - 1) = relation;

    return true;
}

/**
 * @desc Method for reading the file in lp format
 *
 * @returns void
 */
void Interpreter::readLPFile() {
    long long lineNumber = 1;
    string line;
    ostringstream ss;
    bool error, constraints = true;

    //reads the first line
    getline(this->in, line);

    //interprets the first and second line that contains the objective function and mode
    this->getLPObjectiveAndMode(line);

    //the third line must be Subject To
    getline(in, line);

    //reads the rest of the lines containing the restrictions
    while (getline(in, line)) {
        //remove espaços
        line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

        if (line == "Bounds") {
            constraints = false;
            lineNumber++;
            continue;
        } else if (line == "Generals") {
            break;
        }

        if (constraints) {
            //reallocates the constraint matrix and relation vector to receive the new constraint
            this->constraints.conservativeResize(lineNumber, this->objectiveFunction.rows() + 1);
            this->constraints.row(this->constraints.rows() - 1) = VectorXd::Zero(this->objectiveFunction.rows() + 1);
            this->relations.conservativeResize(lineNumber);
            error = !this->getLPConstraint(line);
        } else {
            error = !this->getLPBound(line);
        }

        if (error) {
            ss << lineNumber + 1;
            string message = "Interpreter: Line error " + ss.str() + "!";
            throw (new Exception(message));
        }

        lineNumber++;
    }

}


/**
 * @desc Method to find objective function and lp format mode
 *
 * @param string line the first and second line of the file containing the objective mode and function
 * @returns void
 */
void Interpreter::getLPObjectiveAndMode(string line) {
    string temp;
    long long counter, i;
    double variable;
    unordered_map<long long, double> variables;
    char op = '+';
    bool waitDigit = true;
    bool waitX = true;
    bool waitOperator = false;
    bool waitPos = false;

    //remove spaces
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

    //transforms to uppercase
    transform(line.begin(), line.end(), line.begin(), ::toupper);

    if (line == "MAXIMIZE") {
        this->mode = 2;
    } else if (line == "MINIMIZE") {
        this->mode = 1;
    } else {
        throw (new Exception("Interpreter: No way (Maximize or Minimize) of the objective function has been found!"));
    }

    //reads the second line
    getline(this->in, line);

    //remove spaces
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

    //starts from position 5, since the first 4 are assumed, 'obj:'
    counter = 4;
    variable = 1;

    if (line[counter] == '-') {
        op = '-';
        counter++;
        variable = -variable;
    }

    for (i = counter; i < line.length(); i++) {
        temp = line.substr(counter, i - counter + 1);
        if (waitDigit && isdigit(line[i]) && (!isdigit(line[i + 1]) && line[i + 1] != '.')) {
            variable = atof(temp.c_str());
            counter = i + 1;
            waitDigit = false;
            continue;
        } else if (waitX && toupper(line[i]) == 'X') {
            counter = i + 1;
            waitX = false;
            waitDigit = false;
            waitPos = true;
            continue;
        } else if (i + 1 == line.length() || (waitPos && isdigit(line[i]) && !isdigit(line[i + 1]))) {
            if (!waitPos || waitX) {
                throw (new Exception("Interpreter: Error when interpreting the objective function!"));
            }
            if (op == '-') {
                variable = -variable;
            }
            variables[atoll(temp.c_str())] = variable;
            variable = 1;
            counter = i + 1;
            waitOperator = true;
            waitPos = false;
            continue;
        } else if (waitOperator && (line[i] == '+' || line[i] == '-')) {
            op = line[i];
            counter = i + 1;
            waitOperator = false;
            waitDigit = true;
            waitX = true;
        }
    }

    this->objectiveFunction.resize(variables.size());

    for (i = 0; i < this->objectiveFunction.rows(); i++) {
        this->objectiveFunction(i) = variables[i + 1];
    }
}

/**
* @desc Method for fetching a PLI constraint in lp format
*
* @param string line line of the file containing the constraint, without spaces
* @returns bool true if the constraint was obtained
*/
bool Interpreter::getLPConstraint(string line) {
    string temp;
    long long counter, i;
    double variable;
    unordered_map<long long, double> variables;
    char op = '+';
    int relation;
    bool waitDigit = true;
    bool waitX = true;
    bool waitOperator = false;
    bool waitPos = false;

    //seek the value counter start
    for (i = 0; i < line.length(); i++) {
        if (line[i] == ':') {
            break;
        }
    }

    counter = i + 1;
    variable = 1;

    if (line[counter] == '-') {
        op = '-';
        counter++;
        variable = -variable;
    }

    for (i = counter; i < line.length(); i++) {
        temp = line.substr(counter, i - counter + 1);
        if (waitDigit && isdigit(line[i]) && (!isdigit(line[i + 1]) && line[i + 1] != '.') && i + 1 != line.length()) {
            variable = atof(temp.c_str());
            counter = i + 1;
            waitDigit = false;
            continue;
        } else if (waitX && toupper(line[i]) == 'X') {
            counter = i + 1;
            waitX = false;
            waitDigit = false;
            waitPos = true;
            continue;
        } else if (waitPos && isdigit(line[i]) && !isdigit(line[i + 1]) && i + 1 != line.length()) {
            if (op == '-') {
                variable = -variable;
            }
            variables[atoll(temp.c_str())] = variable;
            variable = 1;
            counter = i + 1;
            waitOperator = true;
            waitPos = false;
            continue;
        } else if (waitOperator && (line[i] == '+' || line[i] == '-') && i + 1 != line.length()) {
            op = line[i];
            counter = i + 1;
            waitOperator = false;
            waitDigit = true;
            waitX = true;
            continue;
        } else if ((line[i] == '>' || line[i] == '=' || line[i] == '<') && i + 1 != line.length()) {
            if (!waitOperator) {
                return false;
            }
            switch (line[i]) {
                case '>':
                    relation = 1;
                    break;
                case '=':
                    relation = 2;
                    break;
                case '<':
                    relation = 0;
                    break;
            }
            if (line[i + 1] == '=') {
                i++;
            }
            if (line[i + 2] == '-') {
                op = '-';
                i++;
            } else {
                op = '+';
            }
            counter = i + 1;
            waitOperator = false;
            continue;
        } else if (i + 1 == line.length()) {
            if (waitOperator || waitDigit || waitPos || waitX) {
                return false;
            }
            variable = atof(temp.c_str());
            if (op == '-') {
                variable = -variable;
            }
            variables[this->objectiveFunction.rows() + 1] = variable;
        }
    }

    for (i = 0; i < this->constraints.cols(); i++) {
        if (variables.find(i + 1) != variables.end()) {
            this->constraints(this->constraints.rows() - 1, i) = variables[i + 1];
        }
    }

    this->relations(this->relations.rows() - 1) = relation;

    return true;
}

/**
* @desc Method for searching the limits of the PLI in lp format
*
* @param string line line of the file containing the constraint, without spaces
* @returns bool true if you managed to get the restriction
*/
bool Interpreter::getLPBound(string line) {
    string temp;
    long long counter, i, pos;
    double variable;
    int relation = -1;
    bool waitDigit = true;
    bool waitX = true;
    bool waitPos = false;

    counter = 0;

    for (i = 0; i < line.length(); i++) {
        temp = line.substr(counter, i - counter + 1);
        if (waitDigit && isdigit(line[i]) && (!isdigit(line[i + 1]) && line[i + 1] != '.') && i + 1 != line.length()) {
            variable = atof(temp.c_str());
            counter = i + 1;
            waitDigit = false;
            waitX = false;
            continue;
        } else if (waitX && toupper(line[i]) == 'X') {
            counter = i + 1;
            waitX = false;
            waitDigit = false;
            waitPos = true;
            continue;
        } else if (waitPos && isdigit(line[i]) && !isdigit(line[i + 1]) && i + 1 != line.length()) {
            pos = atoll(temp.c_str()) - 1;
            if (relation != -1 && variable > 0) {
                //reallocates the constraint matrix and relation vector to receive the new constraint
                this->constraints.conservativeResize(this->constraints.rows() + 1, this->objectiveFunction.rows() + 1);
                this->constraints.row(this->constraints.rows() - 1) = VectorXd::Zero(
                        this->objectiveFunction.rows() + 1);
                this->relations.conservativeResize(this->relations.rows() + 1);
                this->constraints(this->constraints.rows() - 1, pos) = 1;
                this->constraints(this->constraints.rows() - 1, this->constraints.cols() - 1) = variable;
                this->relations(this->relations.rows() - 1) = relation;
            } else {
                relation = -2;
            }
            counter = i + 1;
            waitPos = false;
            continue;
        } else if ((line[i] == '>' || line[i] == '=' || line[i] == '<') && i + 1 != line.length()) {
            if (relation == -1) {
                waitX = true;
            }
            switch (line[i]) {
                case '>':
                    relation = 1;
                    break;
                case '=':
                    relation = 2;
                    break;
                case '<':
                    relation = 0;
                    break;
            }
            if (line[i + 1] == '=') {
                i++;
            }
            counter = i + 1;
            continue;
        } else if (i + 1 == line.length()) {
            if (waitDigit || waitPos || waitX) {
                return false;
            }
            variable = atof(temp.c_str());
            if (variable > 0) {
                //reallocates the constraint matrix and relation vector to receive the new constraint
                this->constraints.conservativeResize(this->constraints.rows() + 1, this->objectiveFunction.rows() + 1);
                this->constraints.row(this->constraints.rows() - 1) = VectorXd::Zero(
                        this->objectiveFunction.rows() + 1);
                this->relations.conservativeResize(this->relations.rows() + 1);
                this->constraints(this->constraints.rows() - 1, pos) = 1;
                this->constraints(this->constraints.rows() - 1, this->constraints.cols() - 1) = variable;
                this->relations(this->relations.rows() - 1) = relation;
            }
        }
    }

    return true;
}

/**
 * @desc Method to return the problem encountered
 *
 * @returns Problem
 */
Problem *Interpreter::getProblem() {
    return this->pli;
}

/**
 * @desc Method to return the mode (Maximization or Minimization)
 *
 * @returns int
 */
int Interpreter::getMode() {
    return this->mode;
}
