#include "headers/Interpreter.h"
#include "headers/BranchBound.h"
#include "headers/CuttingPlane.h"
#include "headers/Exception.h"
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {

    BranchBound *bb = nullptr;
    CuttingPlane *cp = nullptr;
    Interpreter *interpreter = nullptr;
    clock_t time[2];

    try {

        if (argc < 2) {
            throw (Exception("Enter the name of the input file!"));
        }

        interpreter = new Interpreter(argv[1]);

        time[0] = clock();

        bb = new BranchBound(interpreter->getProblem(), interpreter->getMode());

        time[1] = clock();

        double totalTime = (time[1] - time[0]) * 1000.00 / CLOCKS_PER_SEC;

        cout << "----------Branch and Bound---------" << endl;
        if (bb->hasSolution()) {
            cout << "Optimized value: " << bb->getOptimum() << endl;
            cout << "Solution: [" << bb->getSolution().transpose() << "]" << endl;
            cout << "Time: " << totalTime << "ms" << endl;
        } else {
            cout << "Solution not found!" << endl;
        }

        time[0] = clock();

        cp = new CuttingPlane(interpreter->getProblem(), interpreter->getMode());

        time[1] = clock();

        totalTime = (time[1] - time[0]) * 1000.00 / CLOCKS_PER_SEC;

        cout << "----------Cutting Plane---------" << endl;
        if (cp->hasSolution()) {
            cout << "Optimized value: " << cp->getOptimum() << endl;
            cout << "Solution: [" << cp->getSolution().transpose() << "]" << endl;
            cout << "Time: " << totalTime << "ms" << endl;
        } else {
            cout << "Solution not found!" << endl;
        }

    } catch (Exception *ex) {
        ex->print();
    }

    delete bb;
    delete cp;
    delete interpreter;

    return 0;
}
